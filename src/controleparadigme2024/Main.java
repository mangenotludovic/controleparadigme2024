package controleparadigme2024;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Personne> personnes = new ArrayList<>();

        personnes.add(new Membre("Mane", "Limane", 1, new Date(), "Actif"));
        personnes.add(new Membre("Neymar", "Jean", 2, new Date(), "Actif"));

        personnes.add(new Employe("Pennarun", "Justine", 3, "Administateur reseau", 3500.0));
        personnes.add(new Employe("Mangenot", "Ludovic", 4, "Cybersécutité", 4500.0));

       // Livre livre1 = new Livre("Le Petit Prince", "Antoine de Saint-Exupéry", "692900976", true);
       // Livre livre2 = new Livre("1984", "George Orwell", "61696489469", true);

        for (Personne p : personnes) {
            p.afficherDetails();
            System.out.println("---------------------------");
        }
    }
}
