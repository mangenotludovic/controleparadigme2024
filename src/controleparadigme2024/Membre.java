package controleparadigme2024;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Membre extends Personne {
    private Date dateAdhesion;
    private String statut;
    private List<Livre> emprunts;

    public Membre(String nom, String prenom, int id, Date dateAdhesion, String statut) {
        super(nom, prenom, id); 
        this.dateAdhesion = dateAdhesion;
        this.statut = statut;
        this.emprunts = new ArrayList<>(); 
    }

    public Date getDateAdhesion() {
        return dateAdhesion;
    }

    public String getStatut() {
        return statut;
    }

    public List<Livre> getEmprunts() {
        return emprunts;
    }

    public void setDateAdhesion(Date dateAdhesion) {
        this.dateAdhesion = dateAdhesion;
    }

    public void setStatut(String statut) {
        this.statut = statut;
    }

    public void setEmprunts(List<Livre> emprunts) {
        this.emprunts = emprunts;
    }

    public void ajouterEmprunt(Livre livre) {
        emprunts.add(livre);
        livre.emprunter();
    }

    public void retirerEmprunt(Livre livre) {
        emprunts.remove(livre);
        livre.retourner();
    }

    public void afficherEmprunts() {
        if (emprunts.isEmpty()) {
            System.out.println("Aucun emprunt en cours.");
        } else {
            System.out.println("Liste des emprunts :");
            for (Livre livre : emprunts) {
                livre.afficherDetails();
            }
        }
    }

    @Override
    public void afficherDetails() {
        System.out.println("ID: " + getId());
        System.out.println("Nom: " + getNom());
        System.out.println("Prénom: " + getPrenom());
        System.out.println("Date d'adhésion: " + dateAdhesion);
        System.out.println("Statut: " + statut);
        afficherEmprunts();
    }
}


