package controleparadigme2024;

public class Livre implements Empruntable {
    private String titre;
    private String auteur;
    private String isbn;
    private boolean disponible;

    public Livre(String titre, String auteur, String isbn, boolean disponible) {
        this.titre = titre;
        this.auteur = auteur;
        this.isbn = isbn;
        this.disponible = disponible;
    }

    public String getTitre() {
        return titre;
    }

    public String getAuteur() {
        return auteur;
    }

    public String getIsbn() {
        return isbn;
    }

    public boolean isDisponible() {
        return disponible;
    }

    public void setTitre(String titre) {
        this.titre = titre;
    }

    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public void setDisponible(boolean disponible) {
        this.disponible = disponible;
    }

    public void afficherDetails() {
        System.out.println("Titre: " + titre);
        System.out.println("Auteur: " + auteur);
        System.out.println("ISBN: " + isbn);
        System.out.println("Disponible: " + (disponible ? "Oui" : "Non"));
    }

    @Override
    public void emprunter() {
        if (disponible) {
            disponible = false;
            System.out.println("Le livre \"" + titre + "\" a été emprunté.");
        } else {
            System.out.println("Le livre \"" + titre + "\" n'est pas disponible pour emprunt.");
        }
    }

    @Override
    public void retourner() {
        if (!disponible) {
            disponible = true;
            System.out.println("Le livre \"" + titre + "\" a été retourné.");
        } else {
            System.out.println("Le livre \"" + titre + "\" était déjà disponible.");
        }
    }
}



