package controleparadigme2024;

public interface Empruntable {
    void emprunter();
    void retourner();
}

