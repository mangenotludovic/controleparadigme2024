function DNAStrand(dna) {
    const complement = {'A': 'T', 'T': 'A', 'C': 'G', 'G': 'C'};
    
    return dna.replace(/[ATCG]/g, match => complement[match]);
  }
  
  console.log(DNAStrand("ATTGC")); // "TAACG"
  console.log(DNAStrand("GTAT")); // "CATA"
  